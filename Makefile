all: composer.lock analyse

composer.lock: composer.json
	composer update

analyse:
	vendor/bin/phpstan analyse --level max src
