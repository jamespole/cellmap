<?php declare(strict_types=1);

/**
 * Copyright (C) 2019 James Pole
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <https://www.gnu.org/licenses/>.
 *
 * PHP version 7.3
 *
 * @author  James Pole <james@pole.net.nz>
 * @license https://gitlab.com/jamespole/cellmap/blob/master/LICENSE GPL-3.0-or-later
 * @link    https://gitlab.com/jamespole/cellmap
 */

namespace JamesPole\CellMap;

/**
 * A collection of Site objects.
 */
class Bands extends Collection
{


    /**
     * Add a band to this collection.
     *
     * @param Band $band Band to be added to this collection.
     *
     * @return self This object.
     * @throws \RuntimeException When there is already a band with the same ID in the collection.
     */
    public function add(Band $band): self
    {
        if ($this->get($band->getID()) !== null) {
            throw new \RuntimeException('Duplicate ID.');
        }
        $this->objects[] = $band;
        return $this;
    }//end add()


    /**
     * Return the current element.
     *
     * @return Band The current element.
     */
    public function current(): Band
    {
        return $this->objects[$this->key()];
    }//end current()


    /**
     * Get the band with the given ID.
     *
     * @param integer $id ID of the desired band.
     *
     * @return Band The band with the given ID.
     */
    public function get(int $id): ?Band
    {
        foreach ($this->objects as $band) {
            if ($band->getID() === $id) {
                return $band;
            }
        }
        return null;
    }//end get()
}//end class
