<?php declare(strict_types=1);

/**
 * Copyright (C) 2019 James Pole
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <https://www.gnu.org/licenses/>.
 *
 * PHP version 7.3
 *
 * @author  James Pole <james@pole.net.nz>
 * @license https://gitlab.com/jamespole/cellmap/blob/master/LICENSE GPL-3.0-or-later
 * @link    https://gitlab.com/jamespole/cellmap
 */

namespace JamesPole\CellMap;

/**
 * Represents a frequency band.
 */
class Band
{

    /**
     * Band's ID.
     *
     * @var integer
     */
    private $id;

    /**
     * Band's name.
     *
     * @var string
     */
    private $name;


    /**
     * Create a new Band object.
     *
     * @return self A new Band object.
     */
    public static function create(): self
    {
        return new self();
    }//end create()


    /**
     * Get band's ID.
     *
     * @return integer This band's ID.
     */
    public function getID(): int
    {
        return $this->id;
    }//end getID()


    /**
     * Get band's name.
     *
     * @return string This band's name.
     */
    public function getName(): string
    {
        return $this->name;
    }//end getName()


    /**
     * Set band's ID.
     *
     * @param integer $id Band's ID.
     *
     * @return self This object.
     * @throws \RuntimeException When ID is negative.
     */
    public function setID(int $id): self
    {
        if ($id < 0) {
            throw new \RuntimeException('Invalid ID. Must not be negative.');
        }
        $this->id = $id;
        return $this;
    }//end setID()


    /**
     * Set band's name.
     *
     * @param string $name Band's name.
     *
     * @return self This object.
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }//end setName()
}//end class
