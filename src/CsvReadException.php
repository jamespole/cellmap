<?php declare(strict_types=1);

/**
 * Copyright (C) 2019 James Pole
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <https://www.gnu.org/licenses/>.
 *
 * PHP version 7.3
 *
 * @author  James Pole <james@pole.net.nz>
 * @license https://gitlab.com/jamespole/cellmap/blob/master/LICENSE GPL-3.0-or-later
 * @link    https://gitlab.com/jamespole/cellmap
 */

namespace JamesPole\CellMap;

/**
 * Exception thrown when an error is found in a CSV file.
 */
class CsvReadException extends \RuntimeException
{

    /**
     * The file the error occured in.
     *
     * @var \SplFileObject
     */
    private $csvFile;

    /**
     * The line number the error occured in.
     *
     * @var integer
     */
    private $csvLine;


    /**
     * Construct a CsvReadException object.
     *
     * @param \SplFileObject $csvFile The file the error occured in.
     * @param integer        $csvLine The line number the error occured in.
     * @param string         $message Description of the error.
     */
    public function __construct(\SplFileObject $csvFile, int $csvLine, string $message)
    {
        $this->csvFile = $csvFile;
        $this->csvLine = $csvLine;
        parent::__construct($message);
    }//end __construct()


    /**
     * Gets the file where the error occured.
     *
     * @return \SplFileObject File where error occured.
     */
    public function getCsvFile(): \SplFileObject
    {
        return $this->csvFile;
    }//end getCsvFile()


    /**
     * Gets the line number where the error occured.
     *
     * @return integer Line number where the error occured.
     */
    public function getCsvLine(): int
    {
        return $this->csvLine;
    }//end getCsvLine()
}//end class
