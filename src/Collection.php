<?php declare(strict_types=1);

/**
 * Copyright (C) 2019 James Pole
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <https://www.gnu.org/licenses/>.
 *
 * PHP version 7.3
 *
 * @author  James Pole <james@pole.net.nz>
 * @license https://gitlab.com/jamespole/cellmap/blob/master/LICENSE GPL-3.0-or-later
 * @link    https://gitlab.com/jamespole/cellmap
 */

namespace JamesPole\CellMap;

/**
 * Abstract collection of objects.
 *
 * Child classes, extending this abstract class, must implement a current() function. The current()
 * function should declare a specific return type. For example a Cars (plural) collection class
 * should have a current() function that declares a return type of Car (singular).
 *
 * Child classes should implement an add() function. The add() function should declare a specific
 * parameter type. For example a Cars collection class should have a add() function that declares a
 * parameter type of Car.
 *
 * The above practice ensures that static analysis tools can verify that the correct type is being
 * used.
 */
abstract class Collection implements \Countable, \Iterator
{

    /**
     * Iterator's position.
     *
     * @var integer
     */
    private $position;

    /**
     * Internal array.
     *
     * @var array
     */
    protected $objects = [];


    /**
     * Count elements in this collection.
     *
     * @return integer Number of elements in this collection.
     */
    public function count(): int
    {
        return count($this->objects);
    }//end count()


    /**
     * Return the current element.
     *
     * @return object The current element.
     */
    public abstract function current();


    /**
     * Return the key of the current element.
     *
     * @return integer The key of the current element.
     */
    public function key(): int
    {
        return $this->position;
    }//end key()


    /**
     * Move forward to next element.
     *
     * @return self This object.
     */
    public function next(): self
    {
        $this->position++;
        return $this;
    }//end next()


    /**
     * Rewind to the first element.
     *
     * @return self This object.
     */
    public function rewind(): self
    {
        $this->position = 0;
        return $this;
    }//end rewind()


    /**
     * Checks if current position is valid.
     *
     * @return boolean Whether the current position is valid.
     */
    public function valid(): bool
    {
        return isset($this->objects[$this->key()]);
    }//end valid()
}//end class
